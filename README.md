# material-header-nav

Google material 风格的头部导航栏

## 使用方法

出于减小包体积的目的，只支持npm install方式引用

- npm i material-header-nav
- 在前端项目中引入 import material-header-nav
- 在页面中使用<material-header-nav></material-header-nav>

## 特性

此导航栏为响应式导航栏，在屏幕宽度小于1024px时，会隐藏导航区域，显示hamburger按钮。

## 属性

### `active`
- string
- 当前激活的菜单项，与菜单条目中`name`字段对应

### `data`
- array
- 菜单数据
- 示例
```js
[
            {
                name: 'home',
                label: '首页',
            },
            {
                name: 'browser',
                label: '浏览器',
            },
            {
                name: 'feature',
                label: '功能',
                children: [
                    {
                        name: 'introduce',
                        label: '概览',
                    },
                    {
                        name: 'sync',
                        label: '同步',
                    },
                    {
                        name: 'darkmode',
                        label: '深色模式',
                    }
                ],
            },
            {
                name: 'support',
                label: '支持',
                children: [
                    {
                        name: 'tips',
                        label: '实用提示',
                    },
                    {
                        name: 'sup',
                        label: '支持',
                        type: 'link'
                    }
                ],
            },
            {
                name: 'developer',
                label: '开发者',
                type: 'link',
                link: 'https://fileup.cn',
            },
        ]
```

## 事件

### `navclick`
- 当菜单条目被点击时触发
- 回调参数：菜单条目

## 方法

### `hideDrawer`
- 隐藏抽屉菜单