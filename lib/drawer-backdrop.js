import { css } from "lit";
import { html } from "lit-html";

export const DrawerBackdropDom = html`
<div class="drawer-backdrop"></div>
`

export const DrawerBackdropStyle = css`
@media only screen and (min-width: 1024px) {
    .drawer-backdrop {
        display: none;
    }
}
.drawer-backdrop {
    transition: opacity .15s cubic-bezier(0.4, 0, 0.2, 1);
    background: rgba(32, 33, 37, .86);
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    opacity: 0;
    visibility: hidden;
    z-index: 101;
}
`