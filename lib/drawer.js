import { css } from "lit";
import { html } from "lit-html";

export const DrawerCloseSvg = html`
<svg class="drawer-close-icon">
<use xlink:href="#close-white"></use>
</svg>
`

export const DrawerStyle = css`
@media only screen and (min-width: 1024px) {
    .drawer {
        display: none;
    }
}
.drawer {
    background-color: #fff;
    bottom: 0;
    left: 0;
    max-width: 400px;
    min-width: 264px;
    position: fixed;
    right: 58px;
    top: 0;
    transition: transform .35s cubic-bezier(0.24, 1, 0.32, 1);
    transform: translate3d(-100%, 0, 0);
    visibility: hidden;
    z-index: 102;
}
.drawer-close {
    display: block;
    height: 34px;
    position: absolute;
    right: -48px;
    top: 12px;
    width: 34px;
}
.btn-close-drawer {
    height: inherit;
    padding: 0;
    width: inherit;
    background: none;
    border: none;
    cursor: pointer;
}
.drawer-close-icon {
    ursor: pointer;
    height: 24px;
    margin-top: 3px;
    width: 24px;
}
.drawer-content {
    bottom: 0;
    left: 0;
    overflow-y: auto;
    position: absolute;
    right: 0;
    top: 0;
}
.drawer-logo {
    border-bottom: 1px solid #dbdce0;
    height: 64px;
}
.drawer-logo-link {
    margin: 8px 20px;
    position: absolute;
    border-radius: 4px;
    user-select: none;
    align-items: center;
    display: flex;
    height: 48px;
    padding: 0 8px;
}
.drawer-nav-list {
    bottom: 88px;
    left: 0;
    margin: 0;
    overflow-y: auto;
    padding-top: 24px;
    position: absolute;
    right: 0;
    top: 64px;
    width: 100%;
}
.drawer-nav-li {
    cursor: pointer;
    margin: 2px 0;
    padding: 0 8px;
    position: relative;
}
.drawer-nav-li button {
    cursor: pointer;
    max-width: 100%;
    font-size: 1rem;
    color: #5f6368;
}
.drawer-nav-li button:hover {
    color: #202125;
    background-color: #f8f9fb;
}
.drawer-nav-li button svg {
    margin-left: 4px;
    margin-top: 2px;
    transform: none;
    height: 50%;
    transition: all 0.3s ease;
    width: 16px;
    display: inline-block;
    fill: currentColor;
    vertical-align: middle;
}
.drawer-nav-li button svg.external-arrow {
    transition: transform 0.2s ease;
}
.drawer-nav-menu-level-1 {
    align-items: center;
    border-radius: 4px 46px 46px 4px;
    display: flex;
    height: 46px;
    margin-bottom: 1px;
    padding: 0 0 0 12px;
    position: relative;
    width: 100%;
}
.drawer-nav-li button:focus+.drawer-sub-menu {
    max-height: 1000px;
}
.drawer-sub-menu {
    border-left: 2px solid #dbdce0;
    margin-left: 13px;
    max-height: 0px;
    overflow-y: hidden;
    padding: 0;
    transition: max-height .5s cubic-bezier(0, 1, 0.5, 1);
}
.drawer-sub-menu-item {
    cursor: pointer;
    margin: 2px 0;
    padding: 0 8px;
    position: relative;
}
.drawer-sub-menu-item button {
    padding-left: 12px;
    align-items: center;
    border-radius: 4px 46px 46px 4px;
    display: flex;
    height: 46px;
    margin-bottom: 1px;
    padding: 0 0 0 12px;
    position: relative;
    width: 100%;
}
`