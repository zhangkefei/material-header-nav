import { css } from "lit";

export const horizontalNavStyle = css`
.wrapper {
    height: 64px;
    background-color: #fff;
    box-shadow: 0 2px 6px 0 rgb(32 33 37 / 10%);
    display: flex;
    align-items: center;
}
.hamburger {
    display: none;
    height: 100%;
    width: 64px;
}
.hamburger-wrapper {
    display: table-cell;
    height: 100%;
    padding: 12px;
    vertical-align: middle;
}
.hamburger-button {
    appearance: button;
    background: none;
    border-radius: 50%;
    color: inherit;
    display: block;
    font: inherit;
    height: 100%;
    overflow: visible;
    padding: 0;
    position: relative;
    user-select: none;
    width: 100%;
    border: none;
    cursor: pointer;
}
.hamburger-icon {
    height: 24px;
    margin: 0 auto;
    width: 24px;
    fill: #202125;
    display: block;
}
.logo a {
    height: 46px;
    margin-left: 12px;
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 100px;
    transition: background-color 0.3s, color 0.3s;
    cursor: pointer;
    user-select: none;
    border-radius: 4px;
    padding: 0 8px;
}
.logo a:hover {
    background-color: #f8f9fb;
}
nav.h-nav {
    display: block;
    height: 100%;
    margin-left: 40px;
    max-width: 100%;
    color: #5f6368;
}
@media only screen and (max-width: 1023px) {
    nav.h-nav {
        display: none;
    }
    .hamburger {
        display: table;
    }
}
ul {
    padding: 0;
    margin: 0;
}
.nav-list {
    height: 100%;
    overflow: unset;
    display: flex;
    align-items: center;
    list-style: none;
    user-select: none;
}
.nav-list li.nav-item {
    height: 64px;
    display: flex;
    list-style: none;
    align-items: center;
    min-width: max-content;
    position: relative;
    padding: 0 8px;
}
.nav-list li.nav-item.active::after {
    background: #1967d2;
    border-radius: 2px;
    bottom: 0;
    content: '';
    height: 2px;
    left: 0;
    position: absolute;
    right: 0;
    width: 100%;
}
.nav-list li.nav-item a.level-1 {
    border-radius: 4px;
    transition: background-color 0.3s, color 0.3s;
    height: 46px;
    display: flex;
    align-items: center;
    width: 100%;
    cursor: pointer;
    padding: 0 16px;
    max-width: 100%;
}
.nav-list li.nav-item a.level-1:hover {
    color: #202125;
    background-color: #f8f9fb;
}
.nav-list li.nav-item a.level-1:active {
    color: #5f6368;
    background-color: #e9eaee;
}
a svg {
    margin-left: 4px;
    margin-top: 2px;
    transform: none;
    height: 50%;
    transition: all 0.3s ease;
    width: 16px;
    display: inline-block;
    fill: currentColor;
    vertical-align: middle;
}
a svg.external-arrow {
    transition: transform 0.2s ease;
}
a:hover svg.external-arrow {
    transform: translate(4px, -4px);
}
.nav-list li.nav-item .sub-menu {
    background-color: #fff;
    box-shadow: 0 2px 6px 0 rgb(32 33 37 / 10%);
    border-radius: 0 8px 8px;
    display: flex;
    height: auto;
    opacity: 0;
    pointer-events: none;
    overflow: hidden;
    width: max-content;
    display: flex;
    border: 1px solid #dbdce0;
    flex-direction: column;
    left: 8px;
    padding: 6px 0;
    position: absolute;
    top: 54px;
    z-index: 1;
    box-sizing: border-box;
}
.nav-list li.nav-item:hover .sub-menu {
    opacity: 1;
    pointer-events: unset;
}
.nav-list li.nav-item .sub-menu li.sub-menu-item {
    height: 46px;
    margin-bottom: 6px;
    padding: 0 8px;
    box-sizing: border-box;
}
.nav-list li.nav-item .sub-menu li.sub-menu-item a {
    display: flex;
    align-items: center;
    border-radius: 4px;
    transition: background-color 0.3s, color 0.3s;
    height: 46px;
    cursor: pointer;
    padding: 0 12px;
    max-width: 100%;
}
.nav-list li.nav-item .sub-menu li.sub-menu-item a:hover {
    color: #202125;
    background-color: #f8f9fb;
}
.nav-list li.nav-item .sub-menu li.sub-menu-item a:active {
    color: #5f6368;
    background-color: #e9eaee;
}
`