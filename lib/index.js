import { html, css, LitElement, nothing } from 'lit'
import { repeat } from 'lit/directives/repeat.js'
import { classMap } from 'lit/directives/class-map.js'
import { SvgAssets, SvgAssetsStyle, externalArrowSvg, expandSvg, hamburgerSvg } from './svg-assets'
import { horizontalNavStyle } from './h-nav'
import { DrawerBackdropDom, DrawerBackdropStyle } from './drawer-backdrop'
import { DrawerCloseSvg, DrawerStyle } from './drawer'

export class MaterialHeaderNav extends LitElement {

    static properties = {
        data: { type: Array },
        active: { type: String },
    }

    static styles = css`
        button {
            border: none;
            background-color: unset;
        }
        ${SvgAssetsStyle}
        ${horizontalNavStyle}
        ${DrawerBackdropStyle}
        ${DrawerStyle}
    `

    constructor() {
        super()
        this.data = []
        this.active = ''
    }

    render() {
        return html`
            <div class="drawer">
                <div class="drawer-close">
                    <button type="button" class="btn-close-drawer" role="button" @click="${this._onDrawerCloseClick}">
                    ${DrawerCloseSvg}
                    </button>
                </div>
                <div class="drawer-content">
                    <div class="drawer-logo">
                        <div class="drawer-logo-link">
                            <slot name="drawer-logo"></slot>
                        </div>
                    </div>
                    <nav class="drawer-nav">
                        ${this._genDrawerNavList()}
                    </nav>
                </div>
            </div>
            ${DrawerBackdropDom}
            <div class="wrapper">
                <div class="hamburger">
                    <div class="hamburger-wrapper">
                    <button type="button" class="hamburger-button" @click="${this._onHamburgerClick}">
                        ${hamburgerSvg}
                    </button>
                    </div>
                </div>
                <div class="logo">
                    <a>
                        <slot name="main-logo"></slot>
                    </a>
                </div>
                <nav class="h-nav">
                    ${this._genNavList()}
                </nav>
            </div>
            ${SvgAssets}
        `
    }

    hideDrawer() {
        const drawer = this.shadowRoot.querySelector('.drawer')
        const drawerBackdrop = this.shadowRoot.querySelector('.drawer-backdrop')
        drawerBackdrop.style.visibility = 'hidden'
        drawerBackdrop.style.opacity = '0'
        drawer.style.visibility = 'hidden'
        drawer.style.transform = 'translate3d(-100%, 0, 0)'
    }

    _genNavList() {
        return html`
        <ul class="nav-list">
            ${repeat(
                this.data,
                i => html`
                    <li
                        class="${classMap({
                            'nav-item': true,
                            active: i.name === this.active,
                        })}">
                        <a
                            tabindex="0"
                            class="level-1"
                            @click="${() => this._onNavClick(i)}">
                            ${i.label}
                            ${i.type === 'link'
                                ? externalArrowSvg
                                : nothing}
                            ${i.children && i.children.length > 0
                                ? expandSvg
                                : nothing}
                        </a>
                        ${i.children && i.children.length > 0
                            ? this._genSubMenu(i.children)
                            : nothing}
                    </li>
                `
            )}
        </ul>
        `
    }

    _genSubMenu(items) {
        return html`
            <ul class="sub-menu">
                ${repeat(
                    items,
                    i => html`
                        <li class="sub-menu-item">
                            <a
                                tabindex="0"
                                @click="${() => this._onNavClick(i)}">
                                ${i.label}
                                ${i.type === 'link' ? externalArrowSvg : nothing}
                            </a>
                        </li>
                    `
                )}
            </ul>
        `
    }

    _genDrawerNavList() {
        return html`
        <ul class="drawer-nav-list">
            ${repeat(this.data,
                i => html`
                <li class="drawer-nav-li">
                    <button type="button" class="drawer-nav-menu-level-1"  aria-expanded="false"
                    @click="${e => this._onNavClick(i, e)}">
                    ${i.label}
                    ${i.type === 'link'
                                ? externalArrowSvg
                                : nothing}
                    ${i.children && i.children.length > 0
                        ? expandSvg
                        : nothing}
                    </button>
                    ${i.children && i.children.length > 0
                        ? this._genDrawerSubMenu(i.children)
                        : nothing}
                </li>
                `)}
        </ul>
        `
    }

    _genDrawerSubMenu(items) {
        return html`
            <ul class="drawer-sub-menu">
                ${repeat(
                    items,
                    i => html`
                        <li class="drawer-sub-menu-item">
                            <button
                                @click="${() => this._onNavClick(i)}">
                                ${i.label}
                                ${i.type === 'link' ? externalArrowSvg : nothing}
                            </button>
                        </li>
                    `
                )}
            </ul>
        `
    }

    _onHamburgerClick() {
        const drawer = this.shadowRoot.querySelector('.drawer')
        const drawerBackdrop = this.shadowRoot.querySelector('.drawer-backdrop')
        drawerBackdrop.style.visibility = 'visible'
        drawerBackdrop.style.opacity = '0.8'
        drawer.style.visibility = 'visible'
        drawer.style.transform = 'translate3d(0, 0, 0)'
    }

    _onDrawerCloseClick() {
        const drawer = this.shadowRoot.querySelector('.drawer')
        const drawerBackdrop = this.shadowRoot.querySelector('.drawer-backdrop')
        drawerBackdrop.style.visibility = 'hidden'
        drawerBackdrop.style.opacity = '0'
        drawer.style.visibility = 'hidden'
        drawer.style.transform = 'translate3d(-100%, 0, 0)'
    }

    _onNavClick(item) {
        const options = {
            detail: item,
            bubbles: true,
            composed: true,
        }
        this.dispatchEvent(new CustomEvent('navclick', options))
    }
}
customElements.define('material-header-nav', MaterialHeaderNav)
