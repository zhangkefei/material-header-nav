import { css, html } from "lit";

export const SvgAssetsStyle = css`
.svg-assets {
    display: none;
}
`
export const expandSvg = html`
<svg>
    <use xlink:href="#mi-expand"></use>
</svg>
`
export const externalArrowSvg = html`
<svg class="external-arrow">
    <use xlink:href="#arrow-external"></use>
</svg>
`

export const hamburgerSvg = html`
<svg class="hamburger-icon">
<use xlink:href="#hamburger"></use>
</svg>`

export const SvgAssets = html`
<svg class="svg-assets" xmlns="http://www.w3.org/2000/svg">
    <symbol id="hamburger" viewBox="0 0 24 24">
        <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
    </symbol>
    <symbol id="mi-expand" viewBox="0 0 24 24">
        <path
            d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"></path>
    </symbol>
    <symbol id="arrow-external" viewBox="0 0 16 16">
        <path
            d="M6 3.33333V4.66667H10.3933L2.66667 12.3933L3.60667 13.3333L11.3333 5.60667V10H12.6667V3.33333H6Z" />
    </symbol>
    <symbol id="close-white" viewBox="0 0 24 24">
      <path
        d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z"
        fill="white" />
    </symbol>
</svg>
`