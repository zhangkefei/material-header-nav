import { terser } from 'rollup-plugin-terser'
// import cleanup from 'rollup-plugin-cleanup'
// import json from '@rollup/plugin-json'

export default {
    input: 'lib/index.js',
    output: {
        file: 'dist/index.js',
        format: 'esm',
    },
    plugins: [
        terser(),
        // cleanup(),
        // json()
    ],
    external: [
        'lit'
    ]
}
