
console.log('>>> prepare publish...');

const path = require('path')
const fs = require('fs')
const resolve = path.resolve
const projectDir = resolve()
const pkgFile = resolve(projectDir, 'package.json')
const pkg = require(pkgFile)

const buildPkg = JSON.parse(`
{
    "name": "",
    "version": "",
    "description": "",
    "main": "",
    "scripts": {
      "test": "exit 1"
    },
    "author": "zhkf.hunter@icloud.com",
    "license": "ISC",
    "dependencies": {}
}
`)

buildPkg.name = pkg.name
buildPkg.version = pkg.version
buildPkg.description = pkg.description
buildPkg.main = pkg.main
buildPkg.dependencies = pkg.dependencies
buildPkg.author = pkg.author

const distPkgFile = resolve(projectDir, 'dist', 'package.json')

fs.writeFile(distPkgFile, JSON.stringify(buildPkg, null, '    '), err => {
    if (err) {
        console.error(err)
        return
    }
})

const sourceReadmeFile = resolve(projectDir, 'README.md')
const distReadmeFile = resolve(projectDir, 'dist', 'README.md')
fs.writeFileSync(distReadmeFile, fs.readFileSync(sourceReadmeFile));

console.log('>>> prepare complete');
